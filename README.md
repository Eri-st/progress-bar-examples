# Examples for the [Progress Bar](http://u3d.as/bS5) Unity3d asset

Read the full documentation [here](http://eri-st.eu/ProgressBar/Documentation/).

Download the package containing the examples [here](/Examples.unitypackage).

### [Simple example](/SimpleExample/example.cs)

This first example is very simple. It shows how to [get](/SimpleExample/example.cs#example.cs-17) and [set](/SimpleExample/example.cs#example.cs-16) the progress element's value. You can see below that it updates every two seconds (click to watch webm file).

[![SimpleExample.jpg](https://bitbucket.org/repo/9GBgLg/images/2384646558-SimpleExample.jpg)](http://webmshare.com/play/3rnVa)


### [Oven example](/OvenExample)

This second example was written to help a user with a project. It shows three different progress elements in a mock-up of an oven.

[![OvenExample.jpg](https://bitbucket.org/repo/9GBgLg/images/2553719860-OvenExample.jpg)](http://webmshare.com/play/m7L4e)

Here, the first radial element on the left displays the current temperature of the oven, the second one shows the "acceleration" (delta) of the temperature. In the example above, 33% means that at its current rate the progress element fills by one third in [two seconds](/OvenExample/OvenExampleBehaviour.cs#OvenExampleBehaviour.cs-20).

You noticed that both of those elements do not use all of the ring, a [correction](/OvenExample/OvenExampleBehaviour.cs#OvenExampleBehaviour.cs-11) is made in the code to compensate for it.

"Run" starts the oven and it begins cooking (value increases [cubically](/OvenExample/OvenExampleBehaviour.cs#OvenExampleBehaviour.cs-91)), "Pauses" stops that process and lets the oven cool down (decreases [linearly](/OvenExample/OvenExampleBehaviour.cs#OvenExampleBehaviour.cs-88)). "Reset" resets all three elements.