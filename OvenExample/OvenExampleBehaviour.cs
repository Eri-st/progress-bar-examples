﻿using UnityEngine;
using ProgressBar;
using System.Collections;

public class OvenExampleBehaviour : MonoBehaviour {

	[SerializeField] ProgressBarBehaviour CompletionBar;
	[SerializeField] ProgressRadialBehaviour TempRadial;
	[SerializeField] ProgressRadialBehaviour DeltaRadial;

	private float[] radialRange = {0.175f, 0.825f};
	public int TempMax = 300;

	//in Seconds
	public int CookingTime = 60;

	private float curr_Time = 0f;

	//show change in temperature in the last x second(s)
	public float SampleDelta = 2f;

	private bool running = false;
	private bool paused = false;

	void Start () {
		//next three lines link the linear and radial component, searching by the element's name in the hierarchy
		CompletionBar = transform.FindChild("CompletionBar").GetComponent<ProgressBarBehaviour>();
		TempRadial = transform.FindChild("Temp").GetComponent<ProgressRadialBehaviour>();
		DeltaRadial = transform.FindChild("Delta").GetComponent<ProgressRadialBehaviour>();

		//if the previous components couldn't be found, nothing will work of course
		if (!CompletionBar || !TempRadial || !DeltaRadial){
			Debug.LogError("Could not find all Progess components!", this);
		}
		
		//I modified the ProgressRadialBehaviour script to override the way the attached text label is updated
		//this is only because we use only a portion of the element's surface (radialRange)
		TempRadial.UpdateAttachedText = false;
		DeltaRadial.UpdateAttachedText = false;

		//set everything up before beginning
		Reset ();
	}

	void FixedUpdate () {
		//if the oven is on, we increase the elapsed time
		if (running){
			curr_Time += Time.deltaTime;
		}
		//if it is paused, we decreased the elapsed time
		else if (paused){
			curr_Time -= Time.deltaTime;
		}
		//if the oven is off, we do nothing.
		else {return;}

		//we calculate a ratio of the elapsed time by the total time choosen
		float ratio = curr_Time / CookingTime;

		//this ratio is tha completion bar's value, this part is logical and simple enough
		CompletionBar.Value = ratio * 100;

		//the new value of the progressradial element showing the current temperature is set according to a cubic easing function
		TempRadial.Value= EasingFunc(curr_Time, radialRange[0], radialRange[1]-radialRange[0], CookingTime, paused) * 100;
		//we translate the new value into readable percentages
		//note here the correction by the range, this is needed since we are not using 100% of the radial element
		TempRadial.AttachedText.text = Mathf.CeilToInt(((TempRadial.Value/100 - radialRange[0]) / (radialRange[1] - radialRange[0])) * TempMax) + "°c";

		//we now compute the change in temperature in the last seconds (determined by SampleDelta)
		float curr_Delta = TempRadial.Value/100 - EasingFunc(curr_Time - SampleDelta, radialRange[0], radialRange[1]-radialRange[0], CookingTime, paused);
		//we normalized this change to fit inside our radial element
		DeltaRadial.Value = ( curr_Delta / (radialRange[1] - radialRange[0]) + radialRange[0]) * 100;
		//we normalize again but this time as a percentage
		DeltaRadial.AttachedText.text = Mathf.FloorToInt( (curr_Delta / (radialRange[1] - radialRange[0])) * 100) + "%";

		//if the oven timer is done, it stops but doesn't pause
		//This last condition resets most values
		if (curr_Time > CookingTime){
			running = false;
			paused = false;
			DeltaRadial.Value = 0;
			DeltaRadial.AttachedText.text = "0%";
		}	
	}

	public static float EasingFunc(float t, float b, float c, float d, bool paused = false)
	{
		//If paused the oven's temperature will degrade linearly
		if (paused) return c * t / d + b;
		
		//Otherwisse the oven's temperature will increase cubically
		if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
		return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
	}

	//triggerd by "Reset" button
	public void Reset(){
		CompletionBar.Value = 0;
		TempRadial.Value = radialRange[0] * 100;
		DeltaRadial.Value = radialRange[0] * 100;

		curr_Time = 0f;
		
		running = false;
		paused = false;
	}
	//triggered by "Run" button
	public void Run(){
		running = true;
		paused = false;
	}
	//triggered by "Pause" button
	public void Stop(){
		running = false;
		paused = true;
	}
}
























